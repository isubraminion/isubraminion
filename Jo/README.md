#README

##Instructions

To apply the selenium patch for Nutch 1.x, execute the following steps:
 * checkout a fresh copy of Nutch - svn co http://svn.apache.org/repos/asf/nutch/trunk/
 * apply the patch using, patch -p0 -i nutch\_selenium.patch
 * build nutch using, ant or ant runtime


Please note that the patch does not contain modifications to regex-urlfilter.txt and nutch-site.xml
