import re
from parsers.baseParser import BaseParser
from models import *

class FetchParser(BaseParser):

	""" This class parses the dump file in the Fetch folder """
	def __init__(self, text):
		self.text = text
		self.fetchData = None

	def getText(self):
		return self.text

	def setText(self,text):
		self.text = text

	def __preprocess(self):
		data = re.split(r'\n\s*\n', self.text) #Separating  the various crawl datum and (refnos and urls)
		data = [ re.sub(r'\n', ';', record) for record in data ] #creating a list of crawldatum and (refnos and urls)
		data = [x for x in data if x ] #removing empty data points
		data = [refno+';'+crawldatum for refno,crawldatum in zip(data[::2], data[1::2])] #Combining crawldatum with the refns and urls
		data = [re.sub('::','=', datum) for datum in data] #cleaning the data
		data = [re.sub(': ','=', datum) for datum in data] #cleaning the data
		data = [re.sub(',',';', datum) for datum in data]  #removing comma as a delimiting character
		data = [re.sub('\t','', datum) for datum in data] #removing extra spaces
		data = [re.sub('=http','->http',datum) for datum in data]
		return data

	def parse(self):
		data = self.__preprocess()
		fetchData = []
		for item in data:
			try:
				values = dict(record.split('=') for record in item.split(';') if record)
				recno = values['Recno'].strip()
				url = values['URL'].strip()
				version = values['Version'].strip()
				status = values['Status'].strip()
				fetchTime = values['Fetch time'].strip()
				modifiedTime = values['Modified time'].strip()
				retiresSinceFetch = values['Retries since fetch'].strip()
				retryInterval = values['Retry interval'].strip()
				score = values['Score'].strip()
				signature = values['Signature'].strip()
				ngt = values[' _ngt_'].strip()
				contentType = values['Content-Type'].strip()
				pst = values['_pst_'].strip()
				lastModified = values[' lastModified'].strip()
				rs = values['_rs_'].strip()
				metadata = FetchMetadata(ngt, contentType, pst, lastModified, rs)
				crawldatum = CrawlDatum(version, status, fetchTime, modifiedTime, retiresSinceFetch, retryInterval, score, signature, metadata)
				fetchedData = FetchedDataModel(recno, url, crawldatum)
				fetchData.append(fetchedData)
			except:
				pass
		self.fetchData = fetchData

	def analyzeContentType(self):
		if not self.fetchData:
			self.parse()
		contentTypeAnalysis = dict()
		for data in self.fetchData:
			key = data.crawlDatum.metadata.contentType
			if key in contentTypeAnalysis:
				contentTypeAnalysis[key] += 1
			else:
				contentTypeAnalysis[key] = 1
		return contentTypeAnalysis
