from abc import ABCMeta, abstractmethod, abstractproperty


class BaseParser(object):
	"""Abstract Class from which all should inherit"""
	__metaclass__ = ABCMeta

	@abstractmethod
	def parse(self):
		return


