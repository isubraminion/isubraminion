#!/usr/bin/env python
import os
import sys

numberOfArguments = len(sys.argv)
if numberOfArguments != 4:
    print "Usage: analyze <path to nutch bin> <path to segments directory> <path to output directory>"
    sys.exit()

pathToNutch = os.path.join(os.path.abspath(sys.argv[1]), 'nutch')
pathToSegments = os.path.abspath(sys.argv[2])
pathToOutputDirectory = os.path.abspath(sys.argv[3])
pathToMergedSegments = os.path.join(pathToSegments,'*')
cmd_mergeSegs = pathToNutch + " mergesegs " + pathToOutputDirectory + " " + pathToMergedSegments
if os.system(cmd_mergeSegs) != 0:
    print "Exiting analysis..."
    sys.exit()

mergedSegmentName = os.listdir(pathToOutputDirectory)[0]
pathToMergedSegments = os.path.join(pathToOutputDirectory,mergedSegmentName)

parsedContentDirectory = os.path.join(pathToOutputDirectory,'content')
cmd_readseg = pathToNutch + " readseg -dump " + pathToMergedSegments + " " + parsedContentDirectory + " -nofetch -nogenerate -noparse -noparsedata -noparsetext"
os.system(cmd_readseg)

parsedFetchDirectory = os.path.join(pathToOutputDirectory,'fetch')
cmd_readseg = pathToNutch + " readseg -dump " + pathToMergedSegments + " " + parsedFetchDirectory + " -nocontent -nogenerate -noparse -noparsedata -noparsetext"
os.system(cmd_readseg)

parsedGenerateDirectory = os.path.join(pathToOutputDirectory,'generate')
cmd_readseg = pathToNutch + " readseg -dump " + pathToMergedSegments + " " + parsedGenerateDirectory + " -nocontent -nofetch -noparse -noparsedata -noparsetext"
os.system(cmd_readseg)

parsedParseDirectory = os.path.join(pathToOutputDirectory,'parse')
cmd_readseg = pathToNutch + " readseg -dump " + pathToMergedSegments + " " + parsedParseDirectory + " -nocontent -nofetch -nogenerate -noparsedata -noparsetext"
os.system(cmd_readseg)

parsedParseDataDirectory = os.path.join(pathToOutputDirectory,'parsedata')
cmd_readseg = pathToNutch + " readseg -dump " + pathToMergedSegments + " " + parsedParseDataDirectory + " -nocontent -nofetch -nogenerate -noparse -noparsetext"
os.system(cmd_readseg)

parsedParseTextDirectory = os.path.join(pathToOutputDirectory,'parsetext')
cmd_readseg = pathToNutch + " readseg -dump " + pathToMergedSegments + " " + parsedParseTextDirectory + " -nocontent -nofetch -nogenerate -noparse -noparsedata"
os.system(cmd_readseg)


"""
Analyzing Data
"""

pathToDump = os.path.join(parsedFetchDirectory, 'dump')
with open(pathToDump, "r") as dump:
    parsedData = dump.read()

from parsers.fetchParser import FetchParser
fetchParser = FetchParser(parsedData)
contentTypeAnalysis = fetchParser.analyzeContentType()
print contentTypeAnalysis
