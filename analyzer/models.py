class ErrorData(object):
	def __init__(self,url,reason):
		super(ErrorData, self).__init__()
		self.url = url
		self.reason = reason

class FetchedDataModel(object):
	"""docstring for FetchedData"""
	def __init__(self, recno, url, crawlDatum):
		super(FetchedDataModel, self).__init__()
		self.recno = recno
		self.url = url
		self.crawlDatum = crawlDatum

class CrawlDatum(object):
	"""docstring for CrawlDatum"""
	def __init__(
		self, 
		version, 
		status, 
		fetchTime, 
		modifiedTime, 
		retriesSinceFetch,
		retryInterval,
		score,
		signature,
		metadata):
		super(CrawlDatum, self).__init__()
		self.version = version
		self.status = status
		self.fetchTime = fetchTime
		self.modifiedTime = modifiedTime
		self.retriesSinceFetch = retriesSinceFetch
		self.retryInterval = retryInterval
		self.score = score
		self.signature = signature
		self.metadata = metadata

class Metadata(object):
	"""docstring for Metadata"""
	def __init__(self, contentType):
		super(Metadata, self).__init__()
		self.contentType = contentType

class FetchMetadata(Metadata):
	"""docstring for FetchMetadata"""
	def __init__(self, 
		ngt, 
		contentType, 
		pst, 
		lastModified,
		rs):
		super(FetchMetadata, self).__init__(contentType)
		self.ngt = ngt
		self.pst = pst
		self.lastModified = lastModified
		self.rs = rs


