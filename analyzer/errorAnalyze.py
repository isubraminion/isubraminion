#!/usr/bin/env python
import os
import sys

print 'Entering.....'
numberOfArguments = len(sys.argv)
print 'numberOfArguments......'
if numberOfArguments != 3:
    print "Usage: analyze <path to log file> <path to output directory>"
    sys.exit()

pathToLog = os.path.abspath(sys.argv[1])
pathToOutputDirectory = os.path.abspath(sys.argv[2])
print pathToLog,'...........'
print pathToOutputDirectory,'...........'
"""
Analyzing Data
"""

with open(pathToLog, "r") as fo:
	data = fo.read()

from parsers.errorParser import ErrorParser
EParser = ErrorParser(data)
details=EParser.parse()
for k in details:
	print k,' --> ', details[k] 
