#!/usr/bin/env python
import re
import os
import sys
numberOfArguments = len(sys.argv)
if numberOfArguments != 3:	
	print "Usage: analyze <path to dump file> <output folder>"
	sys.exit()
pathToFile = os.path.abspath(sys.argv[1])
pathToOutput= os.path.abspath(sys.argv[2])
with open(pathToFile,"r") as fo:
	data=fo.read()
data= re.split(r'\n',data)
data= [re.sub(r'\t','',record) for record in data]
data= [re.split(' ',record) for record in data]
data= [record for record in data if len(record)==2]
urls=[]
os.chdir(pathToOutput)
for record in data:
	if(record[0]=='URL::'):
		#print record
		if(re.search(r'.jpg',record[1]) or re.search(r'.jpeg',record[1]) or re.search(r'.png',record[1]) or re.search(r'.gif',record[1])):
			print record
			os.system('wget %s'%record[1])
