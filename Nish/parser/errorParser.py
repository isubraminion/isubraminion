import re
from parsers.baseParser import BaseParser
from models import *

class ErrorParser(BaseParser):
	def __init__(self, text):
		self.text = text
	def getText(self):
		return self.text
	def setText(self,text):
		self.text = text
	def __preprocess(self):
		data= re.split(r'\n',self.text)
		data= [re.sub(r'\t','',record) for record in data]
		data= [re.split(' ',record) for record in data]
		data= [record for record in data if len(record)>3]
		return data
	def parse(self):
		errorDetails={}
		seen = set()
		data= self.__preprocess()
		i=0
		url1=''
		cause=''
		for item in data:
			if(item[2]=="ERROR"):
				#print 'Error.......'
				#print item
				cause=item[3]
				regex = re.compile('http:.*')
				url = [x for x in item if regex.match(x)]
				try:
					if url1:
						url1=url[0]
					else:
						url=[x for x in data[i+1] if regex.match(x)]
						url1=url[0]
				except IndexError:
					url = [x for x in data[i+2] if regex.match(x)]
					if url:
						url1=url[0]
				
			i=i+1
			if(url1!='' and cause!=''):
				errorData = ErrorData(url1,cause)
				if url1 not in seen:
					seen.add(url1)
					errorDetails[url1]=cause
		return errorDetails
