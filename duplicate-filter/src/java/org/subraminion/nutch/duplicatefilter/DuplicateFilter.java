package org.subraminion.nutch.duplicatefilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.nutch.util.NutchConfiguration;
import org.apache.hadoop.conf.Configuration;
import org.apache.nutch.net.URLFilter;
import org.apache.nutch.plugin.Extension;
import org.apache.nutch.plugin.PluginRepository;
import org.apache.nutch.util.URLUtil;
import org.apache.nutch.util.domain.DomainSuffix;

import org.apache.nutch.protocol.Content;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Writable;
import org.apache.nutch.segment.SegmentReader;
import org.apache.commons.codec.digest.DigestUtils;
import java.util.*;
import java.io.StringWriter;

public class DuplicateFilter implements URLFilter {

  private static final Logger LOG = LoggerFactory.getLogger(DuplicateFilter.class);
  private Configuration conf;
  private static final String PATH_TO_SEGMENTS = "/mnt/vol1/Varun/mergedsegs/20150226013852";
  private HashMap<String, String> hash = new HashMap<>();
  //////
  public String filter(String url) {
    Content content = readSegment(url);
    if(content != null) {
      String pageContent = new String(content.getContent());
      String messageDigest = DigestUtils.md5Hex(pageContent);
      if(hash.containsKey(messageDigest))
        LOG.info("\nDUPLICATE-FILTER\nDuplicate present at URL: " + url + " with content at URL" + hash.get(messageDigest));
      else
        hash.put(messageDigest, url);
    }
    return url;
  }

  private Content readSegment(String url){
    Text key= new Text(url);
    Path path= new Path(this.PATH_TO_SEGMENTS);
    Content content = null;
    ArrayList<Writable> parsedList = null;
    Map<String,List<Writable>> results=new HashMap<String, List<Writable>>();
    try {
      SegmentReader reader= new SegmentReader(this.conf,true,true,true,true,true,true);
      reader.get(path, key, new StringWriter(), results);
      parsedList=(ArrayList<Writable>) results.get("co");
      Iterator<Writable> parseIter=parsedList.iterator();
      while(parseIter.hasNext()){
        content=(Content) parseIter.next();
      }
    }
    catch (Exception e) {
    }
    return content;
  }

  /** Boilerplate */
  public Configuration getConf() {
    return this.conf;
  }

  /**
   * handles conf assignment and pulls the value assignment from the
   * "urlmeta.tags" property
   */
  public void setConf(Configuration conf) {
    this.conf = conf;
    if (conf == null)
      return;
  }
}
