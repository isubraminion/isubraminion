/**
 * Algorithm to calculate simhash value given a feature vector 
 */
import java.util.ArrayList;
import java.util.Collections;

public class Simhash {

	private static final long primeNumber = 1125899906842597L;
	private ArrayList<Feature> featureVector;

	/*
	 * Calculates a 64 bit hash value for a given string.
	 * @param String: feature whose hash needs to be calculated
	 * return a Long which represents the hash value
	 */
	private Long hash(String string) {
		long h = primeNumber; // prime
		for(int i=0; i<string.length(); i++) {
			h = 31*h + string.charAt(i);
		}
		return h;
	}
	
	/*
	 * Incorporates weights of the features into the computation of simhash 
	 */
	private void computeWeightedHash(Long hashCode, double weight, ArrayList<Double> weightedHashCode) {
		for(int bit=0; bit<64; bit++) {
			if((hashCode & (1L<<bit)) != 0) {
				Double currentWeight = weightedHashCode.get(64-bit-1);
				weightedHashCode.set(64-bit-1, currentWeight + weight);
			}
			else {
				Double currentWeight = weightedHashCode.get(64-bit-1);
				weightedHashCode.set(64-bit-1, currentWeight - weight);
			}
		}
	}
	
	/*
	 * Constructor
	 */
	public Simhash(ArrayList<Feature> featureVector) {
		this.featureVector = featureVector;
	}
	
	/*
	 * Entry point to the calculation of simhash
	 * Return the simhash as a string
	 */
	public String GetSimHash() {
		String simhash = "";
		ArrayList<Double> weightedHashCode = new ArrayList<Double>(Collections.nCopies(64, 0.0));
		for(Feature feature : featureVector) {
			Long hashCode = hash(feature.feature);
			computeWeightedHash(hashCode, feature.weight, weightedHashCode);
		}
		for(int i=0; i<weightedHashCode.size(); i++) {
			if(weightedHashCode.get(i) > 0)
				simhash += "1";
			else
				simhash += "0";
		}
		return simhash;
	}
	
	public static void main(String[] args) {
	   ArrayList<Feature> catFeatures = new ArrayList<Feature>();
	   Feature f = new Feature("Cat", 1.0);
	   catFeatures.add(f);
	   f = new Feature("sat", 0.5);
	   catFeatures.add(f);
	   f = new Feature("on", 0.2);
	   catFeatures.add(f);
	   f = new Feature("mat", 0.75);
	   catFeatures.add(f);
	   Simhash simhash = new Simhash(catFeatures);
	   System.out.println(simhash.GetSimHash());
	   f = new Feature("Dog", 1.0);
	   catFeatures.add(f);
	   f = new Feature("took", 0.5);
	   catFeatures.add(f);
	   f = new Feature("a", 0.2);
	   catFeatures.add(f);
	   f = new Feature("bath", 0.8);
	   catFeatures.add(f);
	   f = new Feature("in", 0.3);
	   catFeatures.add(f);
	   f = new Feature("a", 0.015);
	   catFeatures.add(f);
	   f = new Feature("fridge", 0.75);
	   catFeatures.add(f);
	   simhash = new Simhash(catFeatures);
	   System.out.println(simhash.GetSimHash());
	}

}
